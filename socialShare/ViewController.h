//
//  ViewController.h
//  socialShare
//
//  Created by Omer Aktuna on 6/30/14.
//  Copyright (c) 2014 Omer Aktuna. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

#import "CFShareCircleView.h"


@interface ViewController : UIViewController<CFShareCircleViewDelegate,MFMailComposeViewControllerDelegate>

@property (nonatomic, strong) CFShareCircleView *shareCircleView;
@property (weak, nonatomic) IBOutlet UIButton *btnShare;
@property (weak, nonatomic) IBOutlet UIButton *btnNewPage;
- (IBAction)shareAction:(id)sender;
- (IBAction)newPageAction:(id)sender;

@end
