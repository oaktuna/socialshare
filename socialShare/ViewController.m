//
//  ViewController.m
//  socialShare
//
//  Created by Omer Aktuna on 6/30/14.
//  Copyright (c) 2014 Omer Aktuna. All rights reserved.
//

#import "ViewController.h"
#import "ViewController2.h"

@interface ViewController () {
    BOOL isFacebookNotAvailable;
    BOOL isTwitterNotAvailable;
    BOOL isWhatsappNotAvailable;
}

@end

@implementation ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    _shareCircleView = [[CFShareCircleView alloc] initWithSharers:@[[CFSharer twitter], [CFSharer facebook], [CFSharer mail]]];
    _shareCircleView.delegate = self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CFCircleView delegate methods
- (void)shareCircleView:(CFShareCircleView *)shareCircleView didSelectSharer:(CFSharer *)sharer {
    NSLog(@"Selected sharer: %@", sharer.name);
    
    if ([sharer.name isEqualToString:@"Facebook"]) {
        // Check if the Facebook app is installed and we can present the share dialog
        /*FBLinkShareParams *params = [[FBLinkShareParams alloc] init];
        params.link = [NSURL URLWithString:@"https://developers.facebook.com/docs/ios/share/"];

        
        if ([FBDialogs canPresentShareDialogWithParams:params]) { //if Facebook app installed
            [self presentShareDialog];
        } else {
            [self showFeedDialog];
        }*/
        
        if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
            isFacebookNotAvailable = NO;
            SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
            [controller setInitialText:@"***Facebook Deneme yazısı...***"];
            [controller addURL:[NSURL URLWithString:@"https://www.markafoni.com/product/7865736/"]];
            [controller addImage:[UIImage imageNamed:@"socialsharing-facebook-image.jpg"]];
            [self presentViewController:controller animated:YES completion:Nil];
        }
        else {
            isFacebookNotAvailable = YES;
        }
    }
    else if ([sharer.name isEqualToString:@"Twitter"]) {
        if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
            isTwitterNotAvailable = NO;
            SLComposeViewController *tweetSheet = [SLComposeViewController
                                                   composeViewControllerForServiceType:SLServiceTypeTwitter];
            [tweetSheet setInitialText:@"***Twitter Deneme yazısı...***"];
            [tweetSheet addURL:[NSURL URLWithString:@"https://www.markafoni.com/product/7865736/"]];
            [tweetSheet addImage:[UIImage imageNamed:@"socialsharing-facebook-image.jpg"]];
            [self presentViewController:tweetSheet animated:YES completion:nil];
        }
        else {
            isTwitterNotAvailable = YES;
        }
    }
    else if ([sharer.name isEqualToString:@"Mail"]) {
        isFacebookNotAvailable = NO;
        isTwitterNotAvailable = NO;
        isWhatsappNotAvailable = NO;
        
        MFMailComposeViewController *mailComposeViewController = [[MFMailComposeViewController alloc] init];
        [mailComposeViewController setMailComposeDelegate:self];
        [mailComposeViewController setSubject:@"**Başlık buraya**"];
        
        NSMutableString *body = [NSMutableString string];
        
        [body appendString:@"<p>Gördüğüm bu ürünün ilgini çekebileceğini düşünüyorum.<p>\n"];
        
        [body appendFormat:@"%@\n", @"body buraya geliyor..."];
        
        /*if (feed.description!=nil) {
            [body appendFormat:@"%@", feed.description];
        }
        
        if (feed.ipLink!=nil)
        {
            //[[NSString stringWithFormat:@"%i", cond.maxValue]
            [body appendFormat:@"<a href=\"http://m.mynet.com/index.htm#%@\">Haberin devamı...</a>\n",feed.ipLink];
        }
        
        [body appendString:@"<p></p>"];
        [body appendString:@"<a href=\"http://app.lk/mynetmobil?x=f\">Mynet uygulamasını telefonunuza indirmek için tıklayın!</a>"];*/
        
        
        [mailComposeViewController setMessageBody:body isHTML:YES];
        
        [self presentViewController:mailComposeViewController animated:YES completion:nil];
    }
    else if ([sharer.name isEqualToString:@"WhatsApp"]) {
        NSURL *whatsappURL = [NSURL URLWithString:@"whatsapp://send?text=Hello%2C%20World!"];
        if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
            isWhatsappNotAvailable = NO;
            [[UIApplication sharedApplication] openURL: whatsappURL];
        }
        else{
            isWhatsappNotAvailable = YES;
        }
    }
}

- (void)shareCircleViewDismissed {
    NSString *message;
    UIAlertView *alertView;
    if(isFacebookNotAvailable) {
        message = @"Facebook hesabınızı cihaz üzerine eklemiş olmanız gerekiyor.\n Ayarlar > Facebook\n Sayfasına gidiniz.";
        alertView = [[UIAlertView alloc] initWithTitle:@"Uyarı" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        isFacebookNotAvailable = NO;
    }
    else if (isTwitterNotAvailable) {
        message = @"Twitter hesabınızı cihaz üzerine eklemiş olmanız gerekiyor.\n Ayarlar > Twitter\n Sayfasına gidiniz.";
        alertView = [[UIAlertView alloc] initWithTitle:@"Uyarı" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        isTwitterNotAvailable = NO;
    }
    else if (isWhatsappNotAvailable) {
        message = @"Whatsapp cihazınızda yüklü değil.";
        alertView = [[UIAlertView alloc] initWithTitle:@"Uyarı" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        isWhatsappNotAvailable = NO;
    }
    if(alertView != nil)
        [alertView show];
}

#pragma mark - Facebook helper methods
- (void) presentShareDialog {  //Present share dialog (if Facebook app installed)
    
    // Check if the Facebook app is installed and we can present the share dialog
    FBLinkShareParams *params = [[FBLinkShareParams alloc] init];
    params.link = [NSURL URLWithString:@"https://www.markafoni.com/product/7855297/"];
    
    // If the Facebook app is installed and we can present the share dialog
    if ([FBDialogs canPresentShareDialogWithParams:params]) {
        // Present the share dialog
        
        [FBDialogs presentShareDialogWithLink:params.link
                                      handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
                                          if(error) {
                                              // An error occurred, we need to handle the error
                                              // See: https://developers.facebook.com/docs/ios/errors
                                              NSLog(@"Error publishing story: %@", error.description);
                                          } else {
                                              // Success
                                              NSLog(@"result %@", results);
                                          }
                                      }];
    }
    
}

// Present the feed dialog (if Facebook app not installed)
- (void) showFeedDialog {
    
    // Put together the dialog parameters
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   @"MKFsocialshare iOS app", @"name",
                                   @"MKF DENEME", @"caption",
                                   @"Description alanı...", @"description",
                                   @"https://www.markafoni.com/product/7855297/", @"link",
                                   @"http://i.imgur.com/g3Qc1HN.png", @"picture",
                                   nil];
    
    // Show the feed dialog
    [FBWebDialogs presentFeedDialogModallyWithSession:nil
           parameters:params
              handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                  if (error) {
                      // An error occurred, we need to handle the error
                      // See: https://developers.facebook.com/docs/ios/errors
                      NSLog(@"Error publishing story: %@", error.description);
                  } else {
                      if (result == FBWebDialogResultDialogNotCompleted) {
                          // User cancelled.
                          NSLog(@"User cancelled.");
                      } else {
                          // Handle the publish feed callback
                          NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
                          
                          if (![urlParams valueForKey:@"post_id"]) {
                              // User cancelled.
                              NSLog(@"User cancelled.");
                              
                          } else {
                              // User clicked the Share button
                              NSString *result = [NSString stringWithFormat: @"Posted story, id: %@", [urlParams valueForKey:@"post_id"]];
                              NSLog(@"result %@", result);
                          }
                      }
                  }
              }];
}

// A function for parsing URL parameters returned by the Feed Dialog.
- (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        params[kv[0]] = val;
    }
    return params;
}

#pragma mark - CFShare delegate methods
- (void)shareCircleCanceled:(NSNotification *)notification{
    NSLog(@"Share circle view was canceled.");
}

- (IBAction)shareAction:(id)sender {
    [_shareCircleView showAnimated:YES];
}

- (IBAction)newPageAction:(id)sender {
    [self.navigationController pushViewController:[[ViewController2 alloc] initWithNibName:@"ViewController2" bundle:nil] animated:YES];
}

#pragma mark MFMailComposeViewController Delegate Methods
- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error {
    
    NSLog(@"********EMAIL--------");
    NSString *sonuc;
    
    switch (result) {
        case MFMailComposeResultCancelled:
            NSLog(@"canceled...");
            sonuc = @"İptal edildi.";
            break;
            
        case MFMailComposeResultSaved:
            NSLog(@"saved...");
            sonuc = @"Kayıt edildi.";
            break;
        case MFMailComposeResultSent:
            NSLog(@"sent...");
            sonuc = @"Başarıyla gönderildi.";
            break;
        case MFMailComposeResultFailed:
            NSLog(@"failed...");
            sonuc = @"Gönderilemedi tekrar deneyiniz.";
            break;
        default:
            NSLog(@"not sent...");
            sonuc = @"Gönderilemedi tekrar deneyiniz.";
            break;
            
            
    }
    
//    UIAlertView *info = [[UIAlertView alloc] initWithTitle:@"" message: sonuc  delegate:self cancelButtonTitle:nil otherButtonTitles: @"Tamam", nil];
//    
//    [info show];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


@end
